import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MovieopenComponent } from './movieopen.component';

describe('MovieopenComponent', () => {
  let component: MovieopenComponent;
  let fixture: ComponentFixture<MovieopenComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [MovieopenComponent]
    })
    .compileComponents();
    
    fixture = TestBed.createComponent(MovieopenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
