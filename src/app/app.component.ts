import { Component  , CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { RouterOutlet } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { HomeComponent } from './components/home/home.component';
import { FaqComponent } from './components/faq/faq.component';
import { FooterComponent } from './components/footer/footer.component';
import { ProvideComponent } from './components/provide/provide.component';
import { PlansComponent } from './components/plans/plans.component';
import { ExploreComponent } from './components/explore/explore.component';
import { MovieopenComponent } from './components/movieopen/movieopen.component';

@Component({
  selector: 'app-root',
  standalone: true,
  imports: [RouterOutlet,NavbarComponent,HomeComponent,FaqComponent,FooterComponent,ProvideComponent,PlansComponent,ExploreComponent,MovieopenComponent],
  templateUrl: './app.component.html',
  styleUrl: './app.component.css',
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class AppComponent {
  title = 'streamingweb';
}
